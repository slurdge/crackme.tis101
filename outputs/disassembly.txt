        | IN
        v
+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+
|MOV ACC, 016     |MOV ACC, LEFT    |SUBI ACC, LEFT   |MOV ACC, LEFT    |MOV RIGHT, LEFT  |MOV RIGHT, LEFT  |MOV RIGHT, LEFT  |MOV DOWN, LEFT   |
|f:MOV RIGHT, UP  |MOV RIGHT, ACC   |NEG              |JLZ m            |                 |                 |                 |                 |
|SUBI ACC, 001    |MOV RIGHT, ACC   |MOV RIGHT, ACC   |JMP r            |                 |                 |                 |                 |
|JNZ f            |                 |MOV ACC, LEFT    |m:ADD ACC, 256   |                 |                 |                 |                 |
|s:JMP s          |                 |                 |r:MOV RIGHT, ACC |                 |                 |                 |                 |
+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+
|a:MOV ACC, RIGHT |a:MOV ACC, RIGHT |a:MOV ACC, RIGHT |a:MOV ACC, RIGHT |a:MOV ACC, RIGHT |a:MOV ACC, RIGHT |a:MOV ACC, RIGHT |a:MOV ACC, UP    |
|SUBI ACC, 128    |SUBI ACC, 128    |SUBI ACC, 128    |SUBI ACC, 128    |SUBI ACC, 128    |SUBI ACC, 128    |SUBI ACC, 128    |SUBI ACC, 128    |
|JLZ x            |JLZ B            |JLZ B            |JLZ B            |JLZ B            |JLZ B            |JLZ B            |JLZ B            |
|MOV DOWN, 001    |ADD ACC, ACC     |ADD ACC, ACC     |ADD ACC, ACC     |ADD ACC, ACC     |ADD ACC, ACC     |ADD ACC, ACC     |ADD ACC, ACC     |
|JMP a            |MOV LEFT, ACC    |MOV LEFT, ACC    |MOV LEFT, ACC    |MOV LEFT, ACC    |MOV LEFT, ACC    |MOV LEFT, ACC    |MOV LEFT, ACC    |
|x:MOV DOWN, 000  |MOV DOWN, 001    |MOV DOWN, 001    |MOV DOWN, 001    |MOV DOWN, 001    |MOV DOWN, 001    |MOV DOWN, 001    |MOV DOWN, 001    |
|                 |JMP a            |JMP a            |JMP a            |JMP a            |JMP a            |JMP a            |JMP a            |
|                 |B:ADD ACC, ACC   |B:ADD ACC, ACC   |B:ADD ACC, ACC   |B:ADD ACC, ACC   |B:ADD ACC, ACC   |B:ADD ACC, ACC   |B:ADD ACC, ACC   |
|                 |ADD ACC, 256     |ADD ACC, 256     |ADD ACC, 256     |ADD ACC, 256     |ADD ACC, 256     |ADD ACC, 256     |ADD ACC, 256     |
|                 |MOV LEFT, ACC    |MOV LEFT, ACC    |MOV LEFT, ACC    |MOV LEFT, ACC    |MOV LEFT, ACC    |MOV LEFT, ACC    |MOV LEFT, ACC    |
|                 |MOV DOWN, 000    |MOV DOWN, 000    |MOV DOWN, 000    |MOV DOWN, 000    |MOV DOWN, 000    |MOV DOWN, 000    |MOV DOWN, 000    |
+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+
|MOV ACC, UP      |MOV ACC, UP      |a:MOV ACC, UP    |MOV ACC, UP      |MOV ACC, UP      |MOV ACC, UP      |MOV ACC, UP      |MOV ACC, LEFT    |
|SUBI ACC, RIGHT  |MOV LEFT, ACC    |SUBI ACC, RIGHT  |MOV LEFT, ACC    |SUBI ACC, RIGHT  |MOV LEFT, ACC    |SUBI ACC, 001    |SUBI ACC, 001    |
|JEZ p            |MOV DOWN, ACC    |JEZ u            |MOV RIGHT, ACC   |JGZ m            |MOV DOWN, ACC    |NEG              |NEG              |
|MOV ACC, 001     |                 |MOV DOWN, 000    |NEG              |NEG              |                 |MOV RIGHT, ACC   |SUBI ACC, UP     |
|p:MOV DOWN, ACC  |                 |JMP a            |ADD ACC, 001     |m:SUBI ACC, LEFT |                 |MOV DOWN, ACC    |JEZ x            |
|                 |                 |u:MOV DOWN, 001  |MOV DOWN, ACC    |JGZ w            |                 |                 |MOV ACC, 001     |
|                 |                 |                 |                 |NEG              |                 |                 |x:MOV DOWN, ACC  |
|                 |                 |                 |                 |w:MOV DOWN, ACC  |                 |                 |                 |
+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+
|MOV ACC, 008     |MOV ACC, 008     |MOV ACC, 008     |MOV ACC, 008     |MOV ACC, 008     |MOV ACC, 008     |MOV ACC, 008     |MOV ACC, 008     |
|f:SWP            |f:SWP            |f:SWP            |f:SWP            |f:SWP            |f:SWP            |f:SWP            |f:SWP            |
|ADD ACC, ACC     |ADD ACC, ACC     |ADD ACC, ACC     |ADD ACC, ACC     |ADD ACC, ACC     |ADD ACC, ACC     |ADD ACC, ACC     |ADD ACC, ACC     |
|ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |
|SWP              |SWP              |SWP              |SWP              |SWP              |SWP              |SWP              |SWP              |
|SUBI ACC, 001    |SUBI ACC, 001    |SUBI ACC, 001    |SUBI ACC, 001    |SUBI ACC, 001    |SUBI ACC, 001    |SUBI ACC, 001    |SUBI ACC, 001    |
|JNZ f            |JNZ f            |JNZ f            |JNZ f            |JNZ f            |JNZ f            |JNZ f            |JNZ f            |
|SWP              |SWP              |SWP              |SWP              |SWP              |SWP              |SWP              |SWP              |
|MOV DOWN, ACC    |MOV DOWN, ACC    |MOV DOWN, ACC    |MOV DOWN, ACC    |MOV DOWN, ACC    |MOV DOWN, ACC    |MOV DOWN, ACC    |MOV DOWN, ACC    |
+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+
|MOV RIGHT, UP    |MOV DOWN, ANY    |MOV RIGHT, UP    |MOV DOWN, ANY    |MOV RIGHT, UP    |MOV DOWN, ANY    |MOV RIGHT, UP    |MOV DOWN, ANY    |
|MOV DOWN, RIGHT  |MOV LEFT, ANY    |MOV DOWN, RIGHT  |MOV LEFT, ANY    |MOV DOWN, RIGHT  |MOV LEFT, ANY    |MOV DOWN, RIGHT  |MOV LEFT, ANY    |
+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+
|ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |
|SUBI ACC, 006    |SUBI ACC, 249    |SUBI ACC, 145    |SUBI ACC, 063    |SUBI ACC, 239    |SUBI ACC, 187    |SUBI ACC, 160    |SUBI ACC, 114    |
|JEZ s            |JEZ s            |JEZ s            |JEZ s            |JEZ s            |JEZ s            |JEZ s            |JEZ s            |
|MOV ACC, 001     |MOV ACC, 001     |MOV ACC, 001     |MOV ACC, 001     |MOV ACC, 001     |MOV ACC, 001     |MOV ACC, 001     |MOV ACC, 001     |
|s:MOV DOWN, ACC  |s:MOV DOWN, ACC  |s:MOV DOWN, ACC  |s:MOV DOWN, ACC  |s:MOV DOWN, ACC  |s:MOV DOWN, ACC  |s:MOV DOWN, ACC  |s:MOV DOWN, ACC  |
|SUB ACC, 177     |ADD ACC, 180     |ADD ACC, 017     |SUB ACC, 051     |SUB ACC, 014     |SUB ACC, 058     |ADD ACC, 128     |SUB ACC, 064     |
+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+
|MOV ACC, UP      |MOV ACC, UP      |MOV ACC, UP      |MOV ACC, UP      |MOV ACC, UP      |MOV ACC, UP      |MOV ACC, UP      |MOV ACC, UP      |
|ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |ADD ACC, UP      |
|MOV RIGHT, ACC   |ADD ACC, LEFT    |ADD ACC, LEFT    |ADD ACC, LEFT    |ADD ACC, LEFT    |ADD ACC, LEFT    |ADD ACC, LEFT    |ADD ACC, LEFT    |
|                 |MOV RIGHT, ACC   |MOV RIGHT, ACC   |MOV RIGHT, ACC   |MOV RIGHT, ACC   |MOV RIGHT, ACC   |MOV RIGHT, ACC   |MOV DOWN, ACC    |
+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+
|MOV DOWN, 'H'    |MOV LEFT, 'P'    |MOV LEFT, 'e'    |MOV LEFT, 'y'    |MOV LEFT, 'p'    |MOV LEFT, 'w'    |MOV ACC, RIGHT   |MOV ACC, UP      |
|MOV DOWN, 'e'    |MOV LEFT, 'l'    |MOV LEFT, 'n'    |MOV LEFT, 'o'    |MOV LEFT, 'a'    |MOV LEFT, 'o'    |JNZ Q            |MOV LEFT, ACC    |
|MOV DOWN, 'l'    |MOV LEFT, 'e'    |MOV LEFT, 't'    |MOV LEFT, 'u'    |MOV LEFT, 's'    |MOV LEFT, 'r'    |MOV LEFT, 'G'    |JEZ S            |
|MOV DOWN, 'l'    |MOV LEFT, 'a'    |MOV LEFT, 'e'    |MOV LEFT, 'r'    |MOV LEFT, 's'    |MOV LEFT, 'd'    |MOV LEFT, 'r'    |MOV LEFT, 'F'    |
|MOV DOWN, 'o'    |MOV LEFT, 's'    |MOV LEFT, 'r'    |MOV LEFT, ' '    |u:MOV LEFT, RIGHT|MOV LEFT, ':'    |MOV LEFT, 'e'    |MOV LEFT, 'a'    |
|MOV DOWN, '!'    |MOV LEFT, 'e'    |MOV LEFT, ' '    |z:MOV LEFT, RIGHT|JMP u            |MOV LEFT, ' '    |MOV LEFT, 'a'    |MOV LEFT, 'i'    |
|MOV DOWN, '\n'   |MOV LEFT, ' '    |E:MOV LEFT, RIGHT|JMP z            |                 |E:MOV LEFT, RIGHT|MOV LEFT, 't'    |MOV LEFT, 'l'    |
|J:MOV DOWN, RIGHT|J:MOV LEFT, RIGHT|JMP E            |                 |                 |JMP E            |MOV LEFT, '!'    |MOV LEFT, '!'    |
|JMP J            |JMP J            |                 |                 |                 |                 |MOV LEFT, '\n'   |MOV LEFT, '\n'   |
|                 |                 |                 |                 |                 |                 |Q:MOV LEFT, RIGHT|N:JMP N          |
|                 |                 |                 |                 |                 |                 |JMP Q            |S:MOV LEFT, RIGHT|
|                 |                 |                 |                 |                 |                 |                 |JMP S            |
+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+-----------------+
        | OUT
        v