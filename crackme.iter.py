prompt=b"""================================================================================
           WELCOME TO THE TIS-101 DEVELOPMENT AND TEST ENVIRONMENT
================================================================================
Enter object code followed by NEWLINE:
"""

header="""Bootstrapping system... Done.

Statistics:
  Units: 1
  Peak memory usage: 16

Warning: object code is not authorized, running in insecure mode.

ENVIRONMENT MODE: INSECURE/SANDBOX
Lanching application..."""

import socket
import time
import sys

silent=False

def log(msg, *args, **kargs):
	global silent
	if not silent:
		print("> "+msg, *args, **kargs)

def try_data(data):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect(('marcan.st', 10847))
	s.setblocking(True)
	s.settimeout(1.0)

	def readall():
		r = b''
		while True:
			try:
				data = s.recv(1024)
				if data == b'':
					break
				r += data
			except socket.timeout:
				if r != b'':
					break
				else:
					print('No Data!')
		return r

	def write(data):
		s.sendall(data)

	log('read')
	hello=readall()
	if (hello != prompt):
		log('error', hello, prompt)
	log('write')
	write(bytes(data)+b'\n')
	ans = readall()
	log('write2')
	write(bytes( [103, 114, 49, 100, 99, 48, 109, 80] + [117, 116, 49, 110, 54, 48, 77, 71])) 
	ans += readall()
	log('end')
	return ans

# silent=True
# all=['']*100
# for i in range(100):
# 	#test = '000001005{:02}0800010010077390000'.format(i)
# 	#test = '000000002{:02}'.format(i)
# 	test = '00000001616{:02}717808090011'.format(i)
# 	test = test.encode('ascii')
# 	r = try_data(test)
# 	try:
# 		r = r.decode('ascii')
# 		r = r.replace(header, '')
# 	except:
# 		pass
# 	all[i] = r
# 	print(i,r)
# open('all_opcodes.txt','w').write(str(all))
# sys.exit(0)

while True:
	data = open('test.tis','r').readlines()
	data = "".join([x.split("#")[0] for x in data])
	data = [x for x in data.encode('ascii') if x in b'0123456789']
	r = try_data(data)
	try:
		#print(r.decode('ascii'))
		print(r)
	except:
		print(r)
	c = input('Another run ([Y]/N) ?')
	if c=='n' or c=='N':
		break