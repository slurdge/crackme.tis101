def getbits(b):
	bits=[]
	for i in range(8):
		bits.append(b&1)
		b=b>>1
	return list(reversed(bits))

def makebyte(bits):
	b=0
	for i in range(8):
		b=(b<<1)|bits[i]
	return b

def reverse(bits):
	r=[0]*8
	bits=list(reversed(bits))
	r[1]=bits[1]
	r[3]=(~bits[3])&1
	r[5]=bits[5]
	r[6]=(~bits[6])&1
	r[0]=bits[0]^r[1]
	r[2]=((~bits[2])&1)^r[3]
	r[4]=r[5]^bits[4]^r[3]
	r[7]=bits[7]^r[6]
	return list(reversed(r))

def transpose(lb):
	t=[]
	for i in range(8):
		ti=[0]*8
		for j in range(8):
			ti[j] = lb[j][i]
		t.append(ti)
	return t

def forward(b):
	r=[0]*8
	b=list(reversed(b))
	r[0]=b[0]^b[1]
	r[1]=b[1]
	r[2]=(~(b[2]^b[3]))&1
	r[3]=(~b[3])&1
	r[4]=b[3]^b[4]^b[5]
	r[5]=b[5]
	r[6]=(~b[6])&1
	r[7]=b[6]^b[7]
	return list(reversed(r))

def swap(l):
	l[2],l[3]=l[3],l[2]
	l[6],l[7]=l[7],l[6]

in0=[6, 249, 145, 63, 239, 187, 160, 114]
in1=[177, -180, -17, 51, 14, 58, -128, 64]
in2=[in0[i]+in1[i] for i in range(8)]

solution=[0]
for _in in (in0, in2):
	_in=_in[:]
	swap(_in)
	lb=[getbits(x) for x in list(reversed(_in))]
	a=[]
	for l in transpose(lb):
		a.append(makebyte(reverse(l)))
	for i in range(8):
		solution.append((solution[-1]+a[i])%256)
	
solution=solution[1:]
print(solution)
print(''.join([chr(x) for x in solution]))