import sys
import string

opcodes = """
13: MOV ACC, ANY
14: MOV ACC, LEFT
15: MOV ACC, RIGHT
16: MOV ACC, UP
17: MOV ACC, DOWN
18: MOV ACC, IMM
19: MOV ACC, -IMM
31: MOV ANY, ACC
34: MOV ANY, LEFT
35: MOV ANY, RIGHT
36: MOV ANY, UP
37: MOV ANY, DOWN
41: MOV LEFT, ACC
43: MOV LEFT, ANY
45: MOV LEFT, RIGHT
46: MOV LEFT, UP
47: MOV LEFT, DOWN
48: MOV LEFT, IMM
49: MOV LEFT, -IMM
51: MOV RIGHT, ACC
53: MOV RIGHT, ANY
54: MOV RIGHT, LEFT
56: MOV RIGHT, UP
57: MOV RIGHT, DOWN
58: MOV RIGHT, IMM
59: MOV RIGHT, -IMM
61: MOV UP, ACC
63: MOV UP, ANY
64: MOV UP, LEFT
65: MOV UP, RIGHT
67: MOV UP, DOWN
71: MOV DOWN, ACC
73: MOV DOWN, ANY
74: MOV DOWN, LEFT
75: MOV DOWN, RIGHT
76: MOV DOWN, UP
78: MOV DOWN, IMM
79: MOV DOWN, -IMM
81: ADD ACC, ACC
83: ADD ACC, ANY
84: ADD ACC, LEFT
85: ADD ACC, RIGHT
86: ADD ACC, UP
87: ADD ACC, DOWN
88: ADD ACC, IMM
89: SUB ACC, IMM
90: JMP IMM
91: JEZ IMM
92: JNZ IMM
93: JGZ IMM
94: JLZ IMM
95: SWP
96: NOP?
97: NEG
"""

directions={ '4': 'LEFT', '5': 'RIGHT', '6': 'UP', '7': 'DOWN'}

jmplabels=string.ascii_lowercase+string.ascii_uppercase+string.digits

opcodes = [x.strip() for x in opcodes.split('\n') if x.strip() != '']
opcodes = {x.split(':')[0]: x.split(':')[1].strip() for x in opcodes}

data = open('crackme.separated.iter.tis', 'r').readlines()
cells = [ [ [''] for _ in range(8) ] for _ in range(8) ]
cellslines = [ [ [0] for _ in range(8) ] for _ in range(8) ]
for line in data:
	x,y = int(line[:3]), int(line[3:6])
	line = line[6:].strip()
	numchar = int(line[:3])
	lines=line[3:].split(' ')
	disa = []
	ref = 0
	for line in lines:
		binary=line[:]
		while len(binary)>0:
			nxt=binary[:2]
			dis = jmplabels[ref]+':'
			ref += 2
			if nxt == '98': #special case
				if int(binary[2:3]) < 8:
					dis += 'SUBI ACC, '+directions[binary[2:3]]
					binary=binary[3:]
					ref+=1
				else:
					dis += 'SUBI ACC, '
					if int(binary[2:3]) == 8:
						dis += binary[3:6]
					else:
						dis += '-'+binary[3:6]
					binary=binary[6:]
					ref+=4
			elif nxt in opcodes:
				dis+=opcodes[nxt]
				if 'IMM' in dis:
					char = binary[2:5]
					if chr(int(char)) in string.printable and opcodes[nxt][:3] == 'MOV':
						char = "'"+chr(int(char))+"'"
						if char == "'\n'":
							char = "'\\n'"
					dis=dis.replace('IMM', char)
					binary=binary[5:]
					ref += 3
				elif 'IM1' in dis:
					dis=dis.replace('IM1', binary[2:3])
					binary=binary[3:]
					ref += 1
				else:
					binary=binary[2:]
			else:
				dis+=nxt
				binary=binary[2:]
			disa.append(dis)
	jmptargets=[]
	newdisa=[]
	for line in disa:
		if line.split(':')[1][0]=='J':
			target=int(line[-3:])
			jmptargets.append(jmplabels[target])
			newdisa.append(line.replace(line[-3:], jmplabels[target]))
		else:
			newdisa.append(line)
	disa=[]
	for line in newdisa:
		if line[0] not in jmptargets:
			disa.append(line[2:])
		else:
			disa.append(line)
	cells[x][y] = disa

def pprint(cells):
	r = []
	width = 17
	r.append(' '*int(width/2) + '| IN')
	r.append(' '*int(width/2) + 'v')
	for row in cells:
		delim = ('+'+'-'*width)*len(row)+'+'
		r.append(delim)
		numlines = max([len(x) for x in row])
		for i in range(numlines):
			line = '|'
			for cell in row:
				if i < len(cell):
					line += cell[i].ljust(width)
				else:
					line += ' '*width
				line += '|'
			r.append(line)
	r.append(delim)
	r.append(' '*int(width/2) + '| OUT')
	r.append(' '*int(width/2) + 'v')
	return r

print('\n'.join(pprint(cells)))
open('disassembly.txt','w').write('\n'.join(pprint(cells)))
